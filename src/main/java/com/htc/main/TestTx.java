package com.htc.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.htc.config.SpringConfig;
import com.htc.spring.services.productServices;

public class TestTx {

	public static void main(String[] args) {

		ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

		productServices productService = (productServices) context.getBean("productService");
		
		
		boolean result = productService.removeCategory("Beverages");
		System.out.println(result);
	}
}
