package com.htc.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.htc.config.SpringConfig;
import com.htc.spring.dao.ProductDAO;
import com.htc.spring.entity.Product;

public class Jdbctest {

	public static void main(String[] args) {

		ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
		ProductDAO product = (ProductDAO) context.getBean("productDAO");

		// 1 insert
		//boolean result = product.addProduct(new Product("p000276", "Sprite", "Drinks", 100.0, 50));
		/*
		 * boolean result1 = product.addProduct(new Product("p00014", "Cake", "Bread",
		 * 200.0, 10)); boolean result2 = product.addProduct(new Product("p00016",
		 * "choclate", "choclate", 100.0, 40)); boolean result3 = product.addProduct(new
		 * Product("p00017", "Non_veg", "Non_veg", 50.0, 45)); boolean result4 =
		 * product.addProduct(new Product("p00018", "Veg", "veg", 90.0, 5000));
		 */
	//System.out.println(result);

		
		// 2 Delete
	//	product.removeProduct("p00026");

		
		
		  // 3 Retrive by productcode 
		System.out.println(product.getProduct("p0002"));
		  System.out.println(product.getProduct("p0004"));
		 
		  

		// 4 Retrive All Products
		// System.out.println("-------------------List of Products-----------------");

		// System.out.println(product.getProducts());

		  
		/*
		 * // 5 Update boolean result = product.updateProduct("p0001", 200.0, 90);
		 * 
		 * System.out.println(result);
		 * 
		 */

	}

}
