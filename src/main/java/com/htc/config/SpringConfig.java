package com.htc.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.htc.aspects.LoggingAspect;
import com.htc.spring.dao.CategoryDAOImpl;
import com.htc.spring.dao.ProductDAOImpl;
import com.htc.spring.services.productServices;

@Configuration
@PropertySource(value="classpath:config.properties")
@EnableAspectJAutoProxy
public class SpringConfig {
	

	@Autowired
	Environment env;
	
	@Bean(name="LoggingAspect")
	public LoggingAspect getLoggingAspect() {
		LoggingAspect aspect = new LoggingAspect();
		return aspect;
	}
	
	@Bean(name="dataSource")
	public DriverManagerDataSource getDataSource() {
		DriverManagerDataSource dataSource =new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
		dataSource.setUrl(env.getProperty("jdbc.url"));
		dataSource.setUsername(env.getProperty("jdbc.username"));
		dataSource.setPassword(env.getProperty("jdbc.password"));
		return dataSource;
	}
	
	/*
	 * @Bean(name="dataSource") public DriverManagerDataSource getDataSource() {
	 * DriverManagerDataSource dataSource =new DriverManagerDataSource();
	 * dataSource.setDriverClassName("org.postgresql.Driver");
	 * dataSource.setUrl("jdbc:postgresql://localhost:5432/postgres");
	 * dataSource.setUsername("postgres"); dataSource.setPassword("199612"); return
	 * dataSource; }
	 */
	
	
	
	
	@Bean(name="jdbcTemplate")
	public JdbcTemplate getJdbcTemplate() {
		JdbcTemplate jdbcTemplate =new JdbcTemplate();
		jdbcTemplate.setDataSource(getDataSource());
		return jdbcTemplate;
	}
	
	
	@Bean(name="productDAO")
	public ProductDAOImpl getProductDAO() {
		ProductDAOImpl productDAO = new ProductDAOImpl();
		productDAO.setJdbcTemplate(getJdbcTemplate());
		return productDAO;
	}
	
	
	@Bean(name="categoryDAO")
	public CategoryDAOImpl getCategoryDAO() {
		CategoryDAOImpl categoryDAO = new CategoryDAOImpl();
		categoryDAO.setJdbcTemplate(getJdbcTemplate());
		return categoryDAO;
	}
	
	/*
	 * @Bean(name="productService") public productServices getProductService() {
	 * productServices productService = new productServices();
	 * productService.setCategoryDAO(getCategoryDAO());
	 * productService.setProductDAO(getProductDAO()); return productService; }
	 */
	
}
