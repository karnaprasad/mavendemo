package com.htc.spring.dao;

import com.htc.spring.entity.Category;

public interface CategoryDAO {
	
	public boolean addCategory(Category category);
	public Category getCategory(String category);
	public boolean removeCategory(String category);
}
