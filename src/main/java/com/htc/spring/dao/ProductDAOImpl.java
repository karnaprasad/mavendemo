package com.htc.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.htc.spring.entity.Product;

public class ProductDAOImpl implements ProductDAO {

	JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public boolean addProduct(Product p) {
		boolean addStatus = false;
		int result = jdbcTemplate.update("insert into products values(?,?,?,?,?)", p.getProdutCode(),
				p.getProductDescription(), p.getCategory(), p.getUnitPrice(), p.getQoh());

		if (result == 1)
			addStatus = true;

		return addStatus;
	}

	public Product getProduct(String productCode) {
		Product product = null;
		product = jdbcTemplate.queryForObject(
				"select productCode, productDescription, category, unitPrice, qoh from products where productCode = ?",
				new RowMapper<Product>() {
					public Product mapRow(ResultSet rs, int rownum) throws SQLException {
						System.out.println(rownum);
						Product p = new Product();
						p.setProdutCode(rs.getString(1));
						p.setProductDescription(rs.getString(2));
						p.setCategory(rs.getString(3));
						p.setUnitPrice(rs.getDouble(4));
						p.setQoh(rs.getInt(5));
						return p;
					}
				}, productCode);
		return product;
	}

	public List<Product> getProducts(String category) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Product> getProducts() {
		List<Product> products = new ArrayList<Product>();
		products = jdbcTemplate.query("SELECT productcode, productdescription, category, unitprice, qoh from products",
				new RowMapper<Product>() {
					public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
						System.out.println(rowNum);
						Product pro = new Product();
						pro.setProdutCode(rs.getString(1));
						pro.setProductDescription(rs.getString(2));
						pro.setCategory(rs.getString(3));
						pro.setUnitPrice(rs.getDouble(4));
						pro.setQoh(rs.getInt(5));
						return pro;
					}
				});
		return products;

	}

	public boolean removeProduct(String productCode) {

		/*
		 * boolean removeStatus = false;
		 * 
		 * int status =
		 * jdbcTemplate.update("delete from products where productCode = ?",
		 * productCode); System.out.println("Deleted Record with productCode = " +
		 * productCode );
		 * 
		 * if(status > 0) { removeStatus = true; } return removeStatus; }
		 */

		boolean deleteStatus = false;
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);

		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("pcode", productCode);

		int result = template.update("delete from products where productCode=:pcode", paramMap);

		if (result == 1)
			deleteStatus = true;

		return deleteStatus;
	}

	public boolean updateProduct(String productCode, double unitPrice, int qoh) {
		boolean updateFlag = false;
		if (jdbcTemplate.update("UPDATE products SET unitPrice=?, qoh=? where productCode=?", unitPrice, qoh,
				productCode) == 1) {
			updateFlag = true;
		}
		return updateFlag;
	}

}
