package com.htc.spring.dao;

import java.util.List;

import com.htc.spring.entity.Product;

public interface ProductDAO {
	
	public boolean addProduct(Product p);
	public Product  getProduct(String productCode);
	public List<Product> getProducts(String category); 
	public List<Product> getProducts();
	public boolean removeProduct(String productCode);
	public boolean updateProduct(String productCode, double unitPrice, int qoh);
}
